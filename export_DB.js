const MYSQLDUMP_PATH = ''; // eg.: 'C:\\Program Files\\XAMPP\\mysql\\bin\\mysqldump.exe';
const DB_NAME = '';
const TABLE_NAME = '';
const USER_NAME = '';
const USER_PASSWORD = '';
const OUTPUT_PATH = `.\\${TABLE_NAME}.sql`;

// MySQLDump documentation: https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html
const MYSQLDUMP_COMMAND_PARTS = [
	MYSQLDUMP_PATH,
	'--complete-insert',
	'--comments',
	'--dump-date',
	'--no-create-db',
	// '--no-create-info', // this prevents table creation
	'--add-drop-table',
	'--create-options',
	'--user',
	USER_NAME,
	`--password=${USER_PASSWORD}`,
	DB_NAME,
	TABLE_NAME,
	'>',
	OUTPUT_PATH,
];
const COMMAND = MYSQLDUMP_COMMAND_PARTS.join(' ');

const {exec} = require("child_process");
exec(COMMAND, (error, stdout, stderr) => {
    if (error)
        return console.log(`error: ${error.message}`);

    if (stderr)
        return console.log(`stderr: ${stderr}`);

    console.log(`stdout: ${stdout}`);
});

