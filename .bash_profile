#!/bin/bash

# Linux aliases
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/common-aliases
alias bashreload='source ~/.bash_profile && echo Bash config reloaded' # ubuntu
# alias bashreload='source ~/.bashrc && echo Bash config reloaded' # debian

alias l='ls -aF --almost-all --color=auto --group-directories-first'
alias c='clear && l'
alias sudo='sudo ' # this is so that other commands get expanded
alias grep='grep --color'
alias fgrep='fgrep --color'
alias egrep='egrep --color'

cdl() { cd $1 && l; } # shows directory content after cd
alias cd='cdl'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd /'
alias root='cd /'
alias home='cd ~'
# alias etc="cd /etc/" # these throw me out from the ssh
# alias var="cd /var/" # these throw me out from the ssh

f() { find . -name "*$1*"; } # find some file or directory that contains string, recursively, as a glob
alias count='find . -type f | wc -l' # count files in folder and children
alias duf='du -sh' # show size of files in current location
alias dud='du -d 1 -h' # show size of files at depth 1 in current location

alias r='rm -rf'
alias mkdir='mkdir -pv' #creates parent dirs if needed
alias untar='tar -zxvf'
alias tar='tar -czvf'
alias wget='wget -c ' # continue

alias nowutc='date +"%T %A %d-%m-%Y %B" --utc'
alias nowserver='date +"%T %A %d-%m-%Y %B %Z %z"'
alias now='TZ='Europe/Zagreb' date +"%T %A %d-%m-%Y %Bash %Z %z"'
alias timestamp='date +%s'

getpass() { openssl rand -base64 $1 | tr -d '\n' | cut -b 1-$1; } # generate semi-random pass of some length, use: getpass 20
alias sha256='shasum -a 256'
alias ping='ping -c 5' # limit to 5 pings
alias p='ps -f' # show currently executing processes
alias listen='netstat -tulpn' # shows which processes listen to what

alias dapp='sudo nano /var/discourse/containers/app.yml'
alias dappr='sudo /var/discourse/launcher destroy app && sudo /var/discourse/launcher start app'

# Ask for Git SSH passphrase only once at the start
SSH_KEY_FILE=~/.ssh/id_rsa
if test -f "$SSH_KEY_FILE"; then
	eval "$(ssh-agent -s)"
	ssh-add "$SSH_KEY_FILE"
fi

# Git related aliases
alias top='cd `git rev-parse --show-toplevel`' # go to top folder of Git repository
alias push='git push -u -f origin master'
alias pull='git pull'
alias add='git add .'
alias commit='git commit -m'
alias fetch='git fetch'
alias status='git status'
alias discard='git reset --hard'
alias reset='discard'
alias cache='git rm -r --cached . ; git add .'

# website related aliases
alias site='cd ~/site'
alias public='cd ~/site/public'
alias storage='cd ~/site/storage/app/public'
alias artisan='~/site/artisan'
alias a='artisan'
alias am='a migrate'
# alias composer='set COMPOSER_MEMORY_LIMIT=1100M && php -d memory_limit=-1 ~/composer.phar'
integrity() { echo 'sha384-'$(cat "$1" | openssl dgst -sha384 -binary | openssl base64 -A); } # use like: integrity jquery-3.5.1.slim.min.js
