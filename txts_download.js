const PERSONAL_ACCESS_TOKEN = ''; // create a personal access token with read_api permissions https://gitlab.com/profile/personal_access_tokens
const PROJECT_NUMBER = ''; // found by opening the gitlab repo > settings > general > project ID
const REFERENCE = ''; // commit hash, branch name, or tag name

let filesExcelPath = 'Data/global/excel/';
let filesExcelDownload = [
	"Animdata.txt",
	"Armor.txt",
	"AutoMagic.txt",
	"CharStats.txt",
	"CubeMain.txt",
	"Gems.txt",
	"ItemStatCost.txt",
	"ItemTypes.txt",
	"MagicPrefix.txt",
	"MagicSuffix.txt",
	"Misc.txt",
	"MonStats.txt",
	"Properties.txt",
	"Runes.txt",
	"SetItems.txt",
	"Sets.txt",
	"SkillDesc.txt",
	"Skills.txt",
	"UniqueItems.txt",
	"Weapons.txt",
];
let filesStringsPath = 'Data/local/LNG/ENG/';
let filesStringsDownload = [
	"expansionstring.txt",
	"patchstring.txt",
	"string.txt",
];
// add the file paths to the file names, and create one array from them
filesExcelDownload = filesExcelDownload.map(v => filesExcelPath + v);
filesStringsDownload = filesStringsDownload.map(v => filesStringsPath + v);
const filesDownload = filesExcelDownload.concat(filesStringsDownload);

downloadFiles(REFERENCE, filesDownload);

// ***********************************************************

function downloadFiles(reference, files)
{
	const fs = require('fs');
	const https = require('https');
	const uri_template = `https://gitlab.com/api/v4/projects/${PROJECT_NUMBER}/repository/files/{filePathDownloadEncoded}/raw?ref=${reference}&private_token=${PERSONAL_ACCESS_TOKEN}`;

	const count = files.length;
	let counterDL = 0;
	let counterWrite = 0;
	for (const filePath of files)
	{
		const fileName = filePath.split('/').slice(-1)[0];

		// we have to encode the file path for the gitlab API
		let filePathDownloadEncoded = encodeURIComponent(filePath);
		filePathDownloadEncoded = filePathDownloadEncoded.replace('.', '%2E');

		// we generate the uri from the template by replacing the placeholders with the real data
		const uri = uri_template.replace('{filePathDownloadEncoded}', filePathDownloadEncoded);

		counterDL++;
		console.info(`downloading ${counterDL}/${count}`, fileName);

		// download the files
		https.get(uri, (resp) => {
			if (resp.statusCode !== 200)
			{
				console.error('statusCode', resp.statusCode, 'statusMessage', resp.statusMessage);

				if (resp.headers['content-type'] === 'application/json')
				{
					let data = '';

					resp.on('data', (chunk) => {
						data += chunk;
					});

					resp.on('end', () => {
						try {
							const jsonData = JSON.parse(data);
							console.error('error response', jsonData);
						} catch (e) {
							console.error('error response no json', data);
						}
						process.exit(0);
					});
				}
				else
					process.exit(0);
			}

			let data = '';

			resp.on('data', (chunk) => {
				data += chunk;
			});

			resp.on('end', () => {
				counterWrite++;
				console.info(`writing ${counterWrite}/${count}`, fileName);
				fs.writeFileSync(fileName, data);
			});

		}).on('error', (err) => {
			console.error(`Error on file ${fileName}`, err.message);
			process.exit(0);
		});
	}
}