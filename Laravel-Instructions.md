- Create GitLab project, empty (no readme).
- Install composer using the install-composer.sh script, which installs Composer 2.0 instead of the old composer. This is important since the old one uses too much memory. That causes it to often fail because the server doesn't give us so much memory. The 2.0 version is good though.
- Install Laravel using the install-laravel.sh script. This one gives more memory to composer.
- git init
- git add .
- git commit -m "First commit."
- git config --local user.name "username"
- git config --local user.email "email"
- git remote add origin https://gitlab.com/username/projectname.git
- git push -u -f origin master

- composer require laravel/ui
- artisan ui:auth
- artisan storage:link
- copy .env.example to .env if it's not already there (composer should do it), and add DB credentials, update APP_NAME, APP_URL, add ASSET_URL (APP_URL/storage), then do artisan key:generate
- copy adminer.php to the public folder
- inside app/Provider/AppServiceProvider.php add "use Illuminate\Support\Facades\Schema;" and inside the boot() method add "Schema::defaultStringLength(191);". This fixes the too long key error when migrating.
- artisan migrate
- make a user
- in routes/web.php, change "Auth::routes();" to "Auth::routes(['register' => false]);" to disable registration

- set APP_DEBUG to false in .env when your are production-ready


TODO: find out how to disable access to files starting with a dot.