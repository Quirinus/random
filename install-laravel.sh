#!/bin/sh
# Install Laravel in the "site" directory
# The composer memory limit should be less than the memory available when using the command: free -m
set COMPOSER_MEMORY_LIMIT=750M && php -d memory_limit=-1 composer.phar create-project --prefer-dist laravel/laravel site