const fs = require('fs');

// in these files, delete all columns except these (and move them to the left)
const fileColumnWhitelist = {
	'skills.txt': ['skill', 'Id', 'charclass', 'skilldesc', 'reqlevel', 'anim'],
	'monstats.txt': ['hcIdx', 'NameStr'],
};

// delete all files except these
let fileWhitelistDelete = [
	"Animdata.txt",
	"Armor.txt",
	"AutoMagic.txt",
	"CharStats.txt",
	"CubeMain.txt",
	"expansionstring.txt",
	"Gems.txt",
	"ItemStatCost.txt",
	"ItemTypes.txt",
	"MagicPrefix.txt",
	"MagicSuffix.txt",
	"Misc.txt",
	"MonStats.txt",
	"patchstring.txt",
	"Properties.txt",
	"Runes.txt",
	"SetItems.txt",
	"Sets.txt",
	"SkillDesc.txt",
	"Skills.txt",
	"string.txt",
	"UniqueItems.txt",
	"Weapons.txt",
];
fileWhitelistDelete = fileWhitelistDelete.map(v => v.toLowerCase());

let fileList = fs.readdirSync('.', {encoding: 'utf8', withFileTypes: true});
for (let fileDirent of fileList)
{
	let fileName = fileDirent.name;

	if (!fileDirent.isFile()) continue;
	// if (fileName.endsWith('.js')) continue; // prevents the deletion of the scripts
	if (fileWhitelistDelete.includes(fileName.toLowerCase())) continue;

	fs.unlinkSync(fileName);
}

for (let fileName in fileColumnWhitelist)
{
	const whitelistedColumns = fileColumnWhitelist[fileName];

	const text = fs.readFileSync(fileName, {encoding: 'utf8'});
	const lines = text.split('\n');
	const countLines = lines.length;
	const headers = lines[0].split('\t');
	const countColumns = headers.length;

	let linesOutput = [];

	let columnOrdinals = [];
	for (let i = 0; i < countColumns; i++)
		if (whitelistedColumns.includes(headers[i]))
			columnOrdinals.push(i);

	for (let i = 0; i < countLines; i++)
	{
		linesOutput[i] = [];

		let line = lines[i].split('\t');
		for (let colNum of columnOrdinals)
			linesOutput[i].push(line[colNum]);
		linesOutput[i] = linesOutput[i].join('\t');
	}

	let textOutput = linesOutput.join('\n');

	fs.writeFileSync(fileName, textOutput);
}