<?php

function xcopy($source, $dest, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }
    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }
    // Loop through the folder
    $dir = dir($source);
    while (false !== ($entry = $dir->read())) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }
        // Deep copy directories
        xcopy("{$source}/{$entry}", "{$dest}/{$entry}", $permissions);
    }
    // Clean up
    $dir->close();
    return true;
}

// can copy folder contents into a new folder
// if you want to copy a file into a folder, add the file name at the end of the folder path
$source = 'gg';
$dest = 'includes';
var_dump(xcopy($source, $dest));

?>