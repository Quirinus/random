const PERSONAL_ACCESS_TOKEN = ''; // create a personal access token with read_api permissions https://gitlab.com/profile/personal_access_tokens
const PROJECT_NUMBER = ''; // found by opening the gitlab repo > settings > general > project ID

getLastTagName(function (tagName, commitHash) {
	console.info('tag name', tagName, 'commit hash', commitHash);
});

// **************************************************************

function getLastTagName(callback)
{
	const https = require('https');
	const uri_template = `https://gitlab.com/api/v4/projects/${PROJECT_NUMBER}/repository/tags?private_token=${PERSONAL_ACCESS_TOKEN}`;
	const uri = uri_template;

	let tagName = '';
	let commitHash = '';

	https.get(uri, (resp) => {
		if (resp.statusCode !== 200)
		{
			console.error('statusCode', resp.statusCode, 'statusMessage', resp.statusMessage);

			if (resp.headers['content-type'] === 'application/json')
			{
				let data = '';

				resp.on('data', (chunk) => {
					data += chunk;
				});

				resp.on('end', () => {
					try {
						const jsonData = JSON.parse(data);
						console.error('tag error response', jsonData);
					} catch (e) {
						console.error('tag error response no json', data);
					}
					process.exit(0);
				});
			}
			else
				process.exit(0);
		}

		let data = '';

		resp.on('data', (chunk) => {
			data += chunk;
		});

		resp.on('end', () => {
			try {
				const jsonData = JSON.parse(data);
				tagName = jsonData[0].name;
				commitHash = jsonData[0].commit.id;

				callback(tagName, commitHash);
			} catch (e) {
				console.info('tag error no json', data);
				process.exit(0);
			}
		});

	}).on('error', (err) => {
		console.error('tag error', err.message);
		process.exit(0);
	});
}